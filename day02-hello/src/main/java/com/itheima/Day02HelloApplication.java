package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day02HelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(Day02HelloApplication.class, args);
    }

}
